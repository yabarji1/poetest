#!/usr/bin/env python3
"""Kata file"""


def fusroh(numb: int) -> str:
    """
    FizzBuzz Kata
    """
    ret = ''
    if not numb % 3:
        ret = 'Fus'
    if not numb % 5:
        ret += 'Roh'
    if not numb % 7:
        ret += 'Dah'
    return ret if ret or not ret == '' else str(numb)
