#!/usr/bin/env python3
"""
Executable module init file
"""

__all__ = ["kata", ]

__author__ = "Adrien CHAMINADE"
__copyright__ = "Copyright (C) 2020 Adrien CHAMINADE"
__license__ = "Public Domain"
__version__ = '0.1.0'
