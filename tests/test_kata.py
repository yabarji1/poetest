#!/usr/bin/env python3
"""FUS ROH DAH! kata tests"""

from poetest import kata


class TestsFusRoh:
    """FUSROHDAH! kata tests"""

    def test_fus_3(self):
        """docstring"""
        assert kata.fusroh(3) == "Fus"

    def test_number_4(self):
        """docstring"""
        assert kata.fusroh(4) == "4"

    def test_roh_5(self):
        """docstring"""
        assert kata.fusroh(5) == "Roh"

    def test_fus_6(self):
        """docstring"""
        assert kata.fusroh(6) == "Fus"

    def test_roh_10(self):
        """docstring"""
        assert kata.fusroh(10) == "Roh"

    def test_fusroh_15(self):
        """docstring"""
        assert kata.fusroh(15) == "FusRoh"

    def test_number_17(self):
        """docstring"""
        assert kata.fusroh(17) == "17"

    def test_fusroh_30(self):
        """docstring"""
        assert kata.fusroh(30) == "FusRoh"

    def test_dah_7(self):
        """docstring"""
        assert kata.fusroh(7) == "Dah"

    def test_dah_14(self):
        """docstring"""
        assert kata.fusroh(14) == "Dah"

    def test_fusdah_21(self):
        """docstring"""
        assert kata.fusroh(21) == "FusDah"

    def test_fusdah_42(self):
        """docstring"""
        assert kata.fusroh(42) == "FusDah"

    def test_rohdah_35(self):
        """docstring"""
        assert kata.fusroh(35) == "RohDah"

    def test_rohdah_70(self):
        """dockstring"""
        assert kata.fusroh(70) == "RohDah"

    def test_fusrohdah_105(self):
        """docstring"""
        assert kata.fusroh(105) == "FusRohDah"

    def test_fusrohdah_210(self):
        """docstring"""
        assert kata.fusroh(210) == "FusRohDah"
